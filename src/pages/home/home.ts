import { Component } from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  lat:number;
  lng:number;
  constructor(private location: Geolocation) {
   this.initGeo();
  }
  initGeo(){
    let watch = this.location.watchPosition();
    watch.subscribe((data) => {
      this.lat = data.coords.latitude;
      this.lng = data.coords.longitude;
    });
  }
}
